package main

import "fmt"

func main() {
	var numeroUm float64
	var numeroDois float64
	var numeroTres float64

	fmt.Println("Digite seu primeiro ângulo:")
	fmt.Scan(&numeroUm)
	fmt.Println("Digite seu segundo ângulo:")
	fmt.Scan(&numeroDois)
	fmt.Println("Digite seu terceiro ângulo:")
	fmt.Scan(&numeroTres)

	if numeroUm+numeroDois+numeroTres != 180 {
		fmt.Println("Não é possível formar um triângulo com os ângulos fornecidos")
	} else {
		fmt.Println("É possível formar um triângulo")
	}
}
