package main

import "fmt"

func main() {
	var r, v float64
	fmt.Print("informe o raio de uma esfera qualquer:")
	fmt.Scan(&r)
	v = (r * r * r) * 3.14159 * 4 / 3
	fmt.Printf("o volume da esfera é %.2f", v)
}
