package main

import "fmt"

func main() {
	var n1, n2 float64
	fmt.Print("informe um numero:")
	fmt.Scan(&n1)
	fmt.Print("informe um segundo numero:")
	fmt.Scan(&n2)
	if n1 > n2 {
		fmt.Printf("%.2f é maior que %.2f", n1, n2)
	} else {
		if n2 > n1 {
			fmt.Printf("%.2f é maior que %.2f", n2, n1)
		} else {
			if n1 == n2 {
				fmt.Print("os dois valores são iguais")
			}
		}
	}
}
