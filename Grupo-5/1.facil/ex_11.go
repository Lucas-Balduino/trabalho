package main

import "fmt"

func main() {
	var num int
	var primo bool
	fmt.Print("Dê-me um número: ")
	fmt.Scan(&num)
	for i := 1; i <= num; i++ {
		primo = true
		if (num%i == 0) && (i != 1) && (i != num) {
			fmt.Println(i, num)
			primo = false
			break
		}
	}
	fmt.Println("O número é primo: ", primo)
}
