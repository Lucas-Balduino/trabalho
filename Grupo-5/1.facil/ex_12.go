package main

import "fmt"

func main() {
	var n1, n2, n3, soma, media float64
	var p1, p2, p3 float64
	fmt.Println("-> Notas")
	fmt.Printf("Nota 1: ")
	fmt.Scan(&n1)
	fmt.Printf("Nota 2: ")
	fmt.Scan(&n2)
	fmt.Printf("Nota 3: ")
	fmt.Scan(&n3)
	fmt.Println("-> Pesos")
	fmt.Printf("Peso 1: ")
	fmt.Scan(&p1)
	fmt.Printf("Peso 2: ")
	fmt.Scan(&p2)
	fmt.Printf("Peso 3: ")
	fmt.Scan(&p3)
	soma = (n1 * p1) + (n2 * p2) + (n3 * p3)
	media = (soma) / (p1 + p2 + p3)
	fmt.Printf("Média ponderada: %.2f", media)
}
