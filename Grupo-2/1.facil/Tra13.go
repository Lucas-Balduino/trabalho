package main

import "fmt"

func main() {

	var inicial float64
	var juros float64
	var periodo float64

	fmt.Println("Qual o valor inicial?")
	fmt.Scan(&inicial)
	fmt.Println("Qual a taxa de juros?:")
	fmt.Scan(&juros)
	fmt.Println("Qual o periodo?")
	fmt.Scan(&periodo)

	juros = juros / 100

	fmt.Println("O montante final é igual a", inicial+(inicial*juros*periodo))
}
